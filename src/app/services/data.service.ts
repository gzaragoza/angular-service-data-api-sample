import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private httpClient: HttpClient) {
    console.log('data service works!!!');
  }

  getData() {
    return this.httpClient.get('https://raw.githubusercontent.com/rrafols/mobile_test/master/data.json');
  }

  // get_data_from_api() {
  //   const URL_API = 'https://raw.githubusercontent.com/rrafols/mobile_test/master/data.json';
  //   return this._http.get<CharacterInterface>(URL_API);
  // }



}
